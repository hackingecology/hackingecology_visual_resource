**Nayad_Logo** by Hacking Ecology is licensed under **Creative Commons BY-NC-ND 4.0** <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />

To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-nd/4.0 

Concept & Design by Tiago Maciel