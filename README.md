# Hacking Ecology Visual Resource

Repository with visual identities and resources of Hacking Ecology and our projects. Here you can find detailed info about permission of use.

The visual designs of this repository are licensed using a **Creative Commons BY-NC-ND 4.0** <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />  

### You are free to:  
**Share** — copy and redistribute the material in any medium or format

### Under the following terms:  
**Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

**NonCommercial** — You can copy, distribute, display, perform, and use this material for any purpose other than commercially (unless you get permission first). Non Commercial means not primarily intended for or directed towards commercial advantage or monetary compensation.

**NoDerivatives** — If you remix, transform, or build upon the material, you may not distribute the modified material. But note that simply changing the format does not create a derivative.


For more detail, you can learn more about the **Creative Commons BY-NC-ND 4.0** license [here](https://creativecommons.org/licenses/by-nc-nd/4.0/)